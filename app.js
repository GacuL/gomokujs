$(function() {
    var gameBoard = document.getElementById('game-board');
    var canvas1 = document.getElementById('mid-canvas');
    var canvas2 = document.getElementById('top-canvas');
    const cells = 15;
    var width = gameBoard.attributes.width.value;
    var height = gameBoard.attributes.height.value;

    var controller = new Controller(Board(canvas2, canvas1, gameBoard), Model(cells, width, height));
    
});

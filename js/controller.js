(function(global, $){
var Controller = function(view, model){
  return new Controller.init(view, model)
};

var
    gameStarted = false,
    colorPlayer = "black",
    movesArray = [],
    undoButton = $('#undo'),
    redoButton = $('#redo'),
    currentMove;

Controller.prototype = {
  initBoard: function(){
    var self = this;
    self.view.drawBoard(this.model);
  },
}

Controller.init = function(view, model){
  var self = this;
  self.view = view;
  self.model = model;
  self.initBoard();

  function getMousePos(event) {
     var xPos = Math.round((event.clientX - self.view.getOffsetLeft()) / self.model.cellSize),
         yPos = Math.round((self.model.height - event.clientY + self.view.getOffsetTop() ) / model.cellSize);
     if (xPos > 0 && yPos > 0 && xPos <= model.cells && yPos <= model.cells) {
         return {
             x: xPos,
             y: yPos,
         }
     } else return false;
  }

  view.onMouseMove(function (event) {
      var
          realSquareSize = self.model.getRealCellSize(),

          mousePos = getMousePos(event),
           // div which displays positions

          asciiPosition = mousePos.x + 96; // translate mouse position to proper letter
          self.view.backlight(mousePos, realSquareSize, self.model, colorPlayer);
      if (mousePos !== false) {
          self.view.getInnerHTML(asciiPosition, mousePos);
      }
  }, false);

  view.onMouseClick(function (event) {
      gameStarted = true;
      var
          realSquareSize = self.model.getRealCellSize(),
          mousePos = getMousePos(event);

      if (self.model.checkMove(mousePos, movesArray)){
          self.view.makeMove(mousePos, realSquareSize, self.model, colorPlayer);
          movesArray.push(mousePos);
          currentMove = movesArray[movesArray.length-1];
          colorPlayer = model.nextTurn();
          }
        }, false);

        undoButton.click(function(event){

          var realSquareSize = self.model.getRealCellSize();
          self.view.removeMove(currentMove, realSquareSize, self.model)
          movesArray.pop();
          currentMove = movesArray[movesArray.length-1];

        });
}

Controller.init.prototype = Controller.prototype;
global.Controller = Controller;
})(window, jQuery);

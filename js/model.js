(function(global){
  var Model = function(cells, width, height){
    return new Model.init(cells, width, height);
  };

  var
      turnNo = 1,
      currentPlayer = {},
      arrayOfMoves = [],

      players = [
        {
          name: "Player1",
          color: "black"
        },
        {
          name: "Player2",
          color: "red"
        }
      ];

  Model.prototype ={
    getRealCellSize: function(){
      var realBoardDim = this.cellSize * (this.cells + 1),
          difference = this.width - realBoardDim,
          restForCell = difference / (this.cells + 1);
      return this.cellSize + restForCell;
    },

    nextTurn: function(){
      currentPlayer = players[turnNo % 2].color;
      turnNo += 1;
      return currentPlayer;
    },

    checkMove: function(mousePos, movesArray){
      for(var i = 0; i < movesArray.length; i++){
        if (movesArray[i].x === mousePos.x && movesArray[i].y === mousePos.y){
          return false;
        }
      }
        return true;
      }
    }

Model.init = function(cells, width, height){
  var self = this;
  self.width = width;
  self.height = height;
  self.cells = cells || 15;
  self.cellSize = Math.floor(width / (cells + 1));
  console.log(self.cellSize);
}

Model.init.prototype = Model.prototype;
global.Model = Model;
})(window);

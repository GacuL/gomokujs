(function (global) {
    var Board = function (canvas1, canvas2, canvas3) {
        return new Board.init(canvas1, canvas2,canvas3);
    };

    var status = document.getElementById('status');

    function drawCircle(x, y, radius, context, color, alpha){
      context.beginPath();

      context.arc(x, y, radius, 0, Math.PI * 2, true);
      if (typeof color === 'string' || color instanceof String){
        context.fillStyle = color;
      }
      context.globalAlpha = alpha || 1;
      context.fill();
      context.closePath();
    }

    Board.prototype = {
        getOffsetLeft: function() {
          "use strict"
          return this.context3.canvas.offsetLeft;
        },

        getOffsetTop: function() {
          return this.context3.canvas.offsetTop;
        },

        onMouseMove: function(handler){
          return this.context1.canvas.addEventListener("mousemove", handler);
        },

        onMouseClick: function(handler){
          return this.context1.canvas.addEventListener("click", handler);
        },

        getInnerHTML: function(asciiPosition, mousePos){
          return status.innerHTML = String.fromCharCode(asciiPosition) + mousePos.y;
        },

        drawBoard: function (model) {
          "use strict"

          var squareSize = model.getRealCellSize(); // realDimension of square if dividing has the rest
          console.log(model.width);
          if (model.width !== model.height) {
                throw 'Width and Height are not the same';
          }

          for (var x = squareSize; x < model.width; x += squareSize) {
            this.context3.moveTo(x, squareSize);
            this.context3.lineTo(x, model.height - squareSize);
          }

          for (var y = squareSize; y < model.height; y += squareSize) {
            this.context3.moveTo(squareSize, y);
            this.context3.lineTo(model.width - squareSize, y);
          }
            this.context3.strokeStyle = 'black';
            this.context3.stroke();
            //call the function that draw orientation points
            this.drawLandMarks(this.context3, squareSize, model);
        //counting board size when result of (width/ number of cells) % 2 != 0
        },
        // drawing orientation points on the board
        drawLandMarks: function (context, squareSize, model) {
          var landMark = drawCircle;
          landMark(model.width / 2, model.height / 2, 5, context);
          landMark(squareSize * 4, squareSize * 4, 5, context);
          landMark(model.width - squareSize * 4, squareSize * 4, 5, context);
          landMark(model.width - squareSize * 4, model.height - squareSize * 4, 5, context);
          landMark(squareSize * 4, model.height - squareSize * 4, 5, context);
        },
        //shade under cursor
        backlight: function (mousePos, squareSize, model, colorPlayer){
          var stoneRadius = model.cellSize / 2; // radius of stone
          this.context1.clearRect(0, 0, model.width, model.height); //clearing canvas on every mouse move
          //call the function that drawing circle
          drawCircle(mousePos.x * squareSize, model.height - mousePos.y * squareSize, stoneRadius, this.context1, colorPlayer, 0.5 );
        },

        removeMove: function(currentMove, squareSize, model){
          var stoneRadius = model.cellSize / 2;
          
          //this.context2.clearRect(currentMove.x - stoneRadius, currentMove.y - stoneRadius, stoneRadius * 2, stoneRadius * 2);
          this.context2.clearRect((currentMove.x * squareSize) - stoneRadius - 0.1, model.height - (currentMove.y * squareSize) - stoneRadius - 0.1, squareSize, squareSize);
        },

        makeMove: function(mousePos, squareSize, model, colorPlayer){
          var stoneRadius = model.cellSize / 2; // radius of stone
          //call the function that drawing circle
          //console.log("kolor" + colorPlayer);

          drawCircle(mousePos.x * squareSize, model.height - mousePos.y * squareSize, stoneRadius, this.context2, colorPlayer);
    }
  }
  //canvas1 - topLayer, canvas2 - midLayer, canvas3 - bottomLayer
    Board.init = function (canvas1, canvas2, canvas3) {
        var self = this;
            self.context1 = canvas1.getContext('2d'),
            self.context2 = canvas2.getContext('2d');
            self.context3 = canvas3.getContext('2d');
  };

    Board.init.prototype = Board.prototype;
    global.Board = Board;
})(window);
